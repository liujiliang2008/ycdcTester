/**  
 * @Title: PropParam.java
 * @Package com.icss.trans.cfg
 * @Description: 
 * @author ICSS
 * @date 2015-3-25 下午3:24:17
 * @version V1.0  
 */
package com.icss.trans.cfg;

/**
 * @ClassName: PropParam
 * @Description:
 * @author ICSS
 * @date 2015-3-25 下午3:24:17
 * 
 */
public class PropParam {

    // 传输前置应用服务器IP
    public static final String TRANS_IP = "transIP";

    // 传输前置应用服务器端口
    public static final String TRANS_PORT = "transPort";
}
