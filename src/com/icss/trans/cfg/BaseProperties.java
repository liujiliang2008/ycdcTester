package com.icss.trans.cfg;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @ClassName: BaseProperties
 * @Description:
 * @author ICSS
 * @date 2015-3-25 下午3:16:17
 * 
 */
public class BaseProperties {

    protected static Logger logger = LoggerFactory.getLogger(BaseProperties.class);

    private static final String TRANSPORT_PROPERTIES = "trans.properties";

    private static Map<String, String> propMap;

    private static String transUrl;

    public BaseProperties() {
        putPropertiesInMap();
    }

    /**
     * 
     * @Description: 获取配置参数
     * @param key
     * @return
     */
    public String getProp(String key) {
        return propMap.get(key);
    }

    public String getTransUrl() {
        return transUrl;
    }

    public void setTransUrl(String url) {
    	 transUrl = url;
    }

    public static BaseProperties getInstance() {
        return SingletonHonder.instance;
    }

    /**
     * 
     * @类说明: 静态内部类实现单例
     */
    private static class SingletonHonder {

        private static BaseProperties instance = new BaseProperties();
    }

    private void putPropertiesInMap() {
        InputStream in = null;
        propMap = new HashMap<String, String>();
        try {
            Properties prop = new Properties();
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(TRANSPORT_PROPERTIES);
            prop.load(in);

            Enumeration en = prop.propertyNames();
            while (en.hasMoreElements()) {
                String key = (String) en.nextElement();
                String value = prop.getProperty(key);
                propMap.put(key, value);
            }
            transUrl = "http://" + this.getProp(PropParam.TRANS_IP) + ":" + this.getProp(PropParam.TRANS_PORT) + "/trans";
        } catch (IOException e) {
            logger.error("putPropertiesInMap" + e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
}
