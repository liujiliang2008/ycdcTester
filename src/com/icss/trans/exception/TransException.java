package com.icss.trans.exception;

/**
 * 
 * @ClassName: TransportException
 * @Description: 传输接口使用异常类
 * @author ICSS
 * @date 2015-3-25 下午2:08:23
 * 
 */
public class TransException extends RuntimeException {

    private static final long serialVersionUID = -4995691943024705331L;

    public TransException() {
        super();
    }

    /**
     * message:异常详细信息传递给，异常对象的message属性
     * 
     * @param message
     */
    public TransException(String message) {
        super(message);
    }

    /**
     * cause:异常类，用它来封装原始异常，从而实现，对异常的链式处理。
     * 
     * @param cause
     */
    public TransException(Throwable cause) {
        super(cause);
    }

    /**
     * 异常信息和异常类
     * 
     * @param message
     * @param cause
     */
    public TransException(String message, Throwable cause) {
        super(message, cause);
    }
}
