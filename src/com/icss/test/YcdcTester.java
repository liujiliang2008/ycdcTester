package com.icss.test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.icss.trans.cfg.BaseProperties;
import com.icss.trans.send.httpput.HttpSender;
import com.icss.trans.send.httpput.SendParam;

public class YcdcTester {

    private static File getFile() {  
        //关键是这行...  
        String path = YcdcTester.class.getProtectionDomain().getCodeSource()  
                .getLocation().getFile();  
        try{  
            path = java.net.URLDecoder.decode(path, "UTF-8");//转换处理中文及空格  
        }catch (java.io.UnsupportedEncodingException e){  
            return null;  
        }  
        return new File(path);  
    }

	/**
	 * YcdcTester.jar 10.1.0.1 10000001 cd
	 * cd_201506081211091011_10.1.0.1_10000001.text
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 3) {
			System.err.println("Usage: YcdcTester <local-ip> <remote-orgcode> <trans-type[cd/mq/http]>");
            System.exit(1);
		}
		
		String appIp = args[0];
		String destOrgCode = args[1];
		String transType = args[2].toLowerCase().trim();
		String transTypeStr = "1";
		
		if ("cd".equals(transType)) {
			transTypeStr = "1";
		} else if ("mq".equals(transType)) {
			transTypeStr = "2";
		} else if ("http".equals(transType)) {
			transTypeStr = "4";
		} else {
			System.err.println("Usage: trans-type must in [cd/mq/http]");
			System.exit(1);
		}
		BaseProperties.getInstance().setTransUrl("http://" + appIp + ":8080/trans");
		String base = getFile().getParent();
		String date = new java.text.SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date());
		
		String sendFileName = args[2] + "_" + date + "_" + appIp + "_" + destOrgCode + ".txt";
		File transFile = new File(base + File.separator + "test.txt");

		if (!transFile.exists()) {
            try {
                FileUtils.writeStringToFile(transFile, sendFileName);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
		
		try {
			SendParam param = new SendParam.Builder().sourceAppCode("ICSS_ETL")
                    .destAppCode("ICSS_ETL")
                    .dataTransType("test001")
                    .destOrgCode(destOrgCode)
                    .fileName(sendFileName)
                    .fileInputstream(transFile)
                    .key("key001")
                    .transType(transTypeStr)
                    .build();

            int code = HttpSender.sendData(param);
            if (200 == code) {
				String shellCmd = "ll /trans/rec/ICSS_ETL/test001/ | grep " + sendFileName;
                System.out.println("文件 [" + sendFileName + "]发送成功！");
                System.out.println("请到组织编码为[" + destOrgCode + "]的应用服务器执行命令[ " + shellCmd + " ]下查看文件是否已到达！");
			} else {
				System.err.println("文件发送异常，异常码为：" + code);
			}
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                FileUtils.forceDelete(transFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

}
